<?php
use yii\grid\GridView;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    <p class="lead"> <?=$enunciado?> </p>
    <?=$sql?>
    
</div>
    <?=GridView::widget([
        'dataProvider'=> $resultados,
        'columns'=>$campos
    ]);?>

